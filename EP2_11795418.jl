function digits(n)
	
	resul = zeros(Int, 10)	
	if n == 0
		resul[1] = 1
		return resul
	end

	while n != 0
		dig = n % 10
		resul[dig + 1] += 1
		n = div(n, 10)
	end

	return resul
end

function removeCards(a, b)
	for i in 1:length(b)
		a[i] -= b[i]
		if a[i] < 0
			return false
		end
	end
	
	return true
end

function count(v)
	resul = zeros(Int, 10)
	for i in v
		resul[i + 1] += 1
	end
	return resul
end

function toBase(base, x)
	resul = 0
	pow = 0
	while x != 0
		dig = x % base
		resul += dig * (10 ^ pow)
		x = div(x, base)
		pow += 1
	end
	return resul
end

function checkWill(v)
	counted = count(v)
	digs = digits(6174)
	return removeCards(counted, digs)
end

function checkTaki(v)
	counted = count(v)
	nums = [6174, 7711]
	
	for el in nums
		digs = digits(el)
		if !removeCards(counted, digs)
			return false
		end
	end

	return true
end

function checkJackson(x, v)
	counted = count(v)
	nums = [6174, 7711, x]
	
	for el in nums
		digs = digits(el)
		if !removeCards(counted, digs)
			return false
		end

	end

	return true
end

function checkWillBase(b, v)
	counted = count(v)
	num = toBase(b, 6174)
	digs = digits(num)

	return removeCards(counted, digs)
end

function checkFriends(nums, v)
	counted = count(v)
	push!(nums, 6174)

	resul = true
	for el in nums
		digs = digits(el)
		if !removeCards(counted, digs)
			return nothing
		end

	end
	
	resul = []

	for i in 1:length(counted)
		for j in 1:counted[i]
			push!(resul, i)
		end
	end

	return resul
end
