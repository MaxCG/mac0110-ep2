using Test

include( "EP2_11795418.jl" )

function testDigits()
	println("[Digits] Testing")
	zrs = zeros(Int, 10)
	
	# 1
	temp = zrs[:]
	temp[2] = 1
	@test digits(1) == temp
	
	# 123
	temp = zrs[:]
	temp[2] = 1
	temp[3] = 1
	temp[4] = 1
	@test digits(123) == temp
	
	# 30210
	temp = zrs[:]
	temp[1] = 2
	temp[2] = 1
	temp[3] = 1
	temp[4] = 1
	@test digits(30210) == temp

	# 0
	temp = zrs[:]
	temp[1] = 1
	@test digits(0) == temp
	println("[Digits] OK")
end
	

function testCount()
	println("[Count] Testing")
	zrs = zeros(Int, 10)

	# 1
	temp = zrs[:]
	temp[2] = 1
	@test count([1]) == temp
	
	# 123
	temp = zrs[:]
	temp[2] = 1
	temp[3] = 1
	temp[4] = 1
	@test count([1, 2, 3]) == temp
	
	# 30210
	temp = zrs[:]
	temp[1] = 2
	temp[2] = 1
	temp[3] = 1
	temp[4] = 1
	@test count([3, 0, 2, 1, 0]) == temp

	# 0
	temp = zrs[:]
	temp[1] = 1
	@test count([0]) == temp

	println("[Count] OK")
end

function testRemoveCards()
	println("[Remove] Testing")

	temp = [1, 0, 2]
	@test !removeCards(temp, [0, 0, 3])
	temp = [1, 0, 2]
	@test removeCards(temp, [0, 0, 2]) && temp == [1, 0, 0]
	temp = [1, 1, 2]
	@test removeCards(temp, [1, 0, 2]) && temp == [0, 1, 0]
	
	println("[Remove] OK")
	
end

function testBase()
	println("[Base] Testing")
	@test toBase(2, 10) == 1010
	@test toBase(3, 5) == 12
	@test toBase(4, 6) == 12
	@test toBase(7, 6) == 6
	@test toBase(9, 0) == 0
	println("[Base] OK")
end

function testWill()
	println("[Will] Testing")
	@test !checkWill([1, 0])
	@test checkWill([1, 4, 6, 7])
	@test checkWill([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	println("[Will] OK")
end

function testTaki()
	println("[Taki] Testing")
	@test !checkTaki([1, 0])
	@test !checkTaki([1, 4, 6, 7])
	@test !checkTaki([1, 1, 7, 7])
	@test checkTaki([1, 1, 7, 7, 1, 4, 6, 7])
	println("[Taki] OK")
end

function testJackson()
	println("[Jackson] Testing")
	@test !checkJackson(10, [1, 0])
	@test !checkJackson(42, [1, 4, 6, 7, 1, 1, 7, 7])
	@test !checkJackson(1000, [1, 1, 7, 7, 1, 0, 0, 0])
	@test checkJackson(1234, [6, 1, 7, 4, 7, 7, 1, 1, 1, 2, 3, 4])
	println("[Jackson] OK")
end

function testWillBase()
	println("[WBase] Testing")
	@test !checkWillBase(2, [6, 1, 7, 4])
	@test !checkWillBase(6, [6, 7, 8, 9, 6, 7, 8, 9])
	@test checkWillBase(10, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
	@test checkWillBase(2, [1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0])
	@test checkWillBase(8, [1, 4, 0, 3, 6, 9, 0])
	println("[WBase] OK")
end

function testFriends()
	println("[Friends] Testing")
	@test checkFriends([2], [2]) == nothing
	@test checkFriends([8, 100], [6, 1, 7, 4]) == nothing
	@test checkFriends([42, 333], [3, 3, 3, 4, 2]) == nothing
	@test checkFriends([24, 42, 6174], [2, 4, 4, 2, 6, 1, 7, 4, 6, 1, 7, 4]) == []
	println("[Friends] OK")
end

function test()
	testDigits()
	testCount()
	testRemoveCards()
	testBase()
	testWill()
	testTaki()
	testJackson()
	testWillBase()
	testFriends()
end

test()
