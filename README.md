# Ep2

## Parte 1 - Verifique o numero favorito de Will
Verifique se e possivel formar o numero 6174 apartir dos algarismos fornecidos pelo vetor com checkWill(v)

## Parte 2 - Will e Taki
Da mesma forma verificar se e possivel formar 6174 e 7711 com checkTaki(v)

## Parte 3 - Will, Taki e Jackson
De forma semelhante verificar se e possivel formar 6174, 7711 e x com checkJackson(x, v)

## Parte 4 - Will na base B
Com checkWillBase(b, v) checar se e possivel formar 6174 na base b com os algarismos de v

## Parte 5 (BONUS) - Will e amigos
Com checkFriends(nums, v) verificar se e possivel for os numeros do vetor nums e 6174 com os algarismos de v
